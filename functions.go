package main

import (
	"crypto/md5"
	"encoding/json"
	"encoding/xml"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"fmt"
)

func existsPath(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func cretaeIfNotExist(path string) {
	if !existsPath(path) {
		os.MkdirAll(path, os.ModePerm)
	}
}

func replaceDir(xml_file string, server_files string) []byte {
	b, err := ioutil.ReadFile(xml_file)
	if err != nil {
		Error.Println(err)
	}
	str := string(b)
	str = strings.Replace(str, "http://ftp.drupal.org/files/projects/", server_files, -1)
	str = strings.Replace(str, "https://ftp.drupal.org/files/projects/", server_files, -1)
	return []byte(str)
}

func downloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath+".download")
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
    
	// ToDo Verificar sino esta vacio el archivo
    /* https://stackoverflow.com/questions/17133590/how-to-get-file-length-in-go
     * fi, e := os.Stat("/path/to/file");
if e != nil {
    return e
}
// get the size
size := fi.Size()
     */
	os.Rename(filepath+".download",filepath)
	if err != nil {
		return err
	}

	return nil
}

type Project struct {
	XMLName    xml.Name  `xml:"project" json:"-"`
	Title      string    `xml:"title" json:"title"`
	Short_name string    `xml:"short_name" json:"short_name"`
	Releases   []Release `xml:"releases>release" json:"releases"`
}

type Release struct {
	Name          string `xml:"name" json:"name"`
	Version       string `xml:"version" json:"version"`
	Date          string `xml:"date" json:"date"`
	Download_link string `xml:"download_link" json:"download_link"`
	Mdhash        string `xml:"mdhash" json:"mdhash"`
	Filesize      string `xml:"filesize" json:"filesize"`
	Download      bool   `xml:"-" json:"download"`
}

func getMD5(name string, drupal_version string, version string) string {
	md5 := ""
	name_file := "data/release-history/" + name + "/" + drupal_version
	xmlFile, err := os.Open(name_file)
	if err != nil {
		Error.Println(err)
		return ""
	}
	defer xmlFile.Close()

	b, _ := ioutil.ReadAll(xmlFile)

	var p Project
	xml.Unmarshal(b, &p)

	for _, release := range p.Releases {
		if release.Version == version {
			md5 = release.Mdhash
		}
	}
	return md5
}

//tomado de http://dev.pawelsz.eu/2014/11/google-golang-compute-md5-of-file.html
func md5sum(filePath string) []byte {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return result
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return result
	}

	return hash.Sum(result)
}

func get_list_projects() []byte {
	searchDir := "data/release-history"
	fileList := []string{}
	filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		fileList = append(fileList, path)
		return nil
	})

	projects := []Project{}

	for _, file := range fileList {
		xmlFile, _ := os.Open(file)
		defer xmlFile.Close()
		b, _ := ioutil.ReadAll(xmlFile)
		var p Project
		xml.Unmarshal(b, &p)

		count := 0
		for _, release := range p.Releases {
			download_link := release.Download_link

			p.Releases[count].Download = false
			file_path := "data/ftp.drupal.org/files/projects/" + filepath.Base(download_link)
			if existsPath(file_path) {
				p.Releases[count].Download = true
			}

			//convertir el timetamp en string			
			timestamp, err := strconv.ParseInt(release.Date, 10, 64)
                        // esta dando este error en algunos proyestos 
                        //strconv.ParseInt: parsing "": invalid syntax
			if err != nil {
				Error.Println(err)
			}
			date := time.Unix(timestamp, 10)
			p.Releases[count].Date = date.Format("02/01/2006, 03:04PM")

			//convertir tamaño a formato lejible
			filesize, err := strconv.ParseUint(release.Filesize, 10, 64)
			p.Releases[count].Filesize = ByteSize(filesize)

			//cambiando la direccion de descarga
			download_link = strings.Replace(download_link, "http://ftp.drupal.org/files/projects/", server_files, -1)
			download_link = strings.Replace(download_link, "https://ftp.drupal.org/files/projects/", server_files, 1)
			p.Releases[count].Download_link = download_link

			count = count + 1
		}

		if len(p.Releases) != 0 {
			projects = append(projects, p)
		}

	}

	jsonProjects, _ := json.Marshal(projects)
	return jsonProjects
}


type Config struct {
	AddrServer string `json:"addr_server"`
	ServerFiles string `json:"server_files"`
	Online bool `json:"online"`
}

func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}
