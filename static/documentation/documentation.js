'use strict';

angular.module('mdrupal.documentation', [
    'ui.router',
])

    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('documentation', {
            url: "/documentation",
            templateUrl: 'static/documentation/documentation.html',
            controller: 'DocumentationCtrl'
        });
    }])
    
    

    .controller('DocumentationCtrl', ["$scope", function ($scope) {
        
    }]);