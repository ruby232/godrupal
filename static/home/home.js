'use strict';

angular.module('mdrupal.home', [
    'ui.router',
    'ngResource'
])

    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('home', {
            url: "/home",
            templateUrl: 'static/home/home.html',
            controller: 'HomeCtrl'
        });
    }])
    .controller('HomeCtrl', ["$scope", 'spinnerService', '$http', function ($scope, spinnerService, $http) {

        spinnerService.show('containerSpinner');
        $scope.projects = []
        $scope.online = true

        $http.get('/api/projects')
            .success(function (data, status, headers, config) {
                $scope.projects = data;
                spinnerService.hide('containerSpinner');

            })
            .error(function (data, status, header, config) {
                console.log(data);
                spinnerService.hide('containerSpinner');

            });

        $http.get('/api/get-online')
            .success(function (data, status, headers, config) {
                $scope.online = data.online;

            })
            .error(function (data, status, header, config) {
                console.log(data);
            });

        $scope.changeOnline = function () {

            $http.get('/api/set-online?online='+$scope.online)
                .success(function (data, status, headers, config) {
                    console.log($scope.online);
                })
                .error(function (data, status, header, config) {
                    console.log(data);
                });
        };
    }]);