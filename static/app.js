'use strict';

// Declare app level module which depends on views, and components
angular.module('mdrupal', [
    'ui.router',
    'angularSpinners',
    'mdrupal.home',
    'mdrupal.documentation',
    'mdrupal.version'
])
    .config(['$urlRouterProvider', function ($urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
    }])
;
