'use strict';

describe('mdrupal.version module', function() {
  beforeEach(module('mdrupal.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
