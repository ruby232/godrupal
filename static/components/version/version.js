'use strict';

angular.module('mdrupal.version', [
  'mdrupal.version.interpolate-filter',
  'mdrupal.version.version-directive'
])

.value('version', '0.1');
