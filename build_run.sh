echo "Build app"
env GOOS=linux CGO_ENABLED=0 go build index.go bytefmt.go functions.go log.go

echo "Accept port 80"
sudo setcap 'cap_net_bind_service=+ep' $(pwd)/index

./index