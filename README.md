# Ejecutar el servidor
```sh
./index
```

Si no se ejecuta porque no tiene permisos para abrir el puerto 80, ejecutar
```sh
setcap 'cap_net_bind_service=+ep' /path/to/program
```

# Compilar
```sh
```
