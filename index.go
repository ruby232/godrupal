package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"fmt"
)

var (
	server_files = "http://localhost:8081/ftp.drupal.org/files/projects/"
	online       = true
	addr_server = ":8080"
)

func index(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "templates/index.html")
}

//Ejemplo de URL release-history/views/7.x
func release_history(w http.ResponseWriter, r *http.Request) {
	xml_file := "data" + r.URL.Path
	arg := strings.Split(r.URL.Path, "/")
	var proj string
	if len(arg) > 2 {
		proj = arg[2]
	}

	proj_pach := "data/release-history/" + proj
	cretaeIfNotExist(proj_pach)

	if online {
		url := "https://updates.drupal.org" + r.URL.Path
		err := downloadFile(xml_file, url)
		if err != nil {
			Error.Println(err)
		}
	}

	response := replaceDir(xml_file, server_files)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(response)
}

// URL Original http://ftp.drupal.org/files/projects/views-7.x-3.13.tar.gz
// /ftp.drupal.org/files/projects/*.tar.gz
func get_project(w http.ResponseWriter, r *http.Request) {
	path_file := "data" + r.URL.Path
	arg := strings.Split(r.URL.Path, "/")
	name_file := arg[4]

	reg, _ := regexp.Compile(`(.+?)-`)
	name := reg.FindAllStringSubmatch(name_file, -1)[0][1]
	if name == "drupal"{
		reg, _ = regexp.Compile(`-(\d)\.*`)
	}else{
		reg, _ = regexp.Compile(`(\d\.x)-`)
	}
	drupal_version := reg.FindAllStringSubmatch(name_file, -1)[0][1]
	reg, _ = regexp.Compile(`(.+?)-(.*)(\.tar\.gz)`)
	version := reg.FindAllStringSubmatch(name_file, -1)[0][2]
	update := true

	//Comprobar si es un dev y esta descargado
	reg, _ = regexp.Compile(`.*\-dev\.tar\.gz`)
	if existsPath(path_file) && reg.MatchString(name_file) {
		mdhash_xml := getMD5(name, drupal_version, version)
		mdhash_file := string(md5sum(path_file))
		if mdhash_xml != mdhash_file {
			if online {
				update = false
				/*Info.Println("Borrando " + path_file + " porque esta desactualizado.")
				var err = os.Remove(path_file)
				if err != nil {
					Error.Println(err)
				}*/
			} else {
				Info.Println("El archivo " + path_file + " esta descatualizado pero no se va a borrar porque no hay coneccion.")
			}
		}
	}

	if !existsPath(path_file) || !update {
		url := "https:/" + r.URL.Path
		Info.Println("Descargando " + url)
		err := downloadFile(path_file,url)
		if err != nil {
			Error.Println(err)
		}
	}

	if existsPath(path_file) {
		w.Header().Set("Content-Disposition", "attachment; filename="+name_file)
		w.Header().Set("Content-Type", "tar.gz")
		//w.Header().Set("Transfer-Encoding", "chunked")
		http.ServeFile(w, r, path_file)
	} else {
		Error.Println("No existe el fichero " + path_file)
	}
}

// https://ftp.drupal.org/files/translations/8.x/addtoany/addtoany-8.x-1.0-rc1.af.po
func get_translation(w http.ResponseWriter, r *http.Request) {
	path_file := "data" + r.URL.Path
	arg := strings.Split(r.URL.Path, "/")
	name_file := arg[5]

	if !existsPath(path_file) {
		url := "https:/" + r.URL.Path
		downloadFile(url, path_file)
	}

	w.Header().Set("Content-Disposition", "attachment; filename="+name_file)
	w.Header().Set("Content-Type", "application/octet-stream")
	//w.Header().Set("Last-Modified", "")
	http.ServeFile(w, r, path_file)
}

func json_get_projects(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json_projects := get_list_projects()
	w.Write(json_projects)
}

func siwch_online(w http.ResponseWriter, r *http.Request) {
	str_online := r.URL.Query().Get("online")
	online, _ = strconv.ParseBool(str_online)

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte(`{"online":` + strconv.FormatBool(online) + `}`))
}

func get_online(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte(`{"online":` + strconv.FormatBool(online) + `}`))
}

func main() {
	//Inicializano los logs
	LogInit(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)

	//Take values fron file of config
	if existsPath("config.json") {
		fmt.Println("Load configuration from config.json")
		config := LoadConfiguration("config.json")
		addr_server = config.AddrServer
		server_files = config.ServerFiles
		online = config.Online

	}

	http.HandleFunc("/", index)
	http.HandleFunc("/release-history/", release_history)
	http.HandleFunc("/ftp.drupal.org/files/projects/", get_project)
	http.HandleFunc("/ftp.drupal.org/files/translations", get_translation)
	http.HandleFunc("/api/projects", json_get_projects)
	http.HandleFunc("/api/set-online", siwch_online)
	http.HandleFunc("/api/get-online", get_online)

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	fmt.Println("Server listen by address " + addr_server)
	log.Fatal(http.ListenAndServe(addr_server, nil))

}
