#!/usr/bin/env bash

cp godrupal.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start godrupal.service
